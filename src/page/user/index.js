import React from 'react';

import UserContainer from '../../container/user-container';

function UserPage() {
	return (
		<div className="PageManagement">
			<h2>User Management</h2>
			<UserContainer />
		</div>
	);
}

export default UserPage;
