import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Header from '../component/header';
import Navigator from '../component/navigator';

export default class Master extends Component {
	static propTypes = {
		children: PropTypes.object.isRequired,
	}

	render() {
		return (
			<div className="PageWrapper">
				<Header />
				<main className="PageContainer container-fluid">
					<div className="row">
						<aside className="col-2 PageAside">
							<div className="PageNavigatorWrapper">
								<Navigator />
							</div>
						</aside>
						<main className="col-10 PageMain">
							{this.props.children}
						</main>
					</div>
				</main>
			</div>
		);
	}
}