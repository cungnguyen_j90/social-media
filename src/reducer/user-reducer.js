import {
	REQUEST_USERS,
	RECEIVE_USERS,
	ERROR_USERS,
} from '../action/user-action';

const initialState = {
	isFetching: false,
	users: [],
};

export default function usersReducer(state = initialState, action) {
	switch (action.type) {
		case REQUEST_USERS:
			return {
				...state,
				isFetching: true,
			};
		case RECEIVE_USERS:
			return {
				...state,
				isFetching: false,
				users: action.users,
			};
		case ERROR_USERS:
			return {
				...state,
				isFetching: false,
				errorUser: action.errorUser,
			};
		default:
			return state;
	}
}
