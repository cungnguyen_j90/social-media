import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchComments } from '../action/comment-action';
import CommentItem from '../component/comment-item';

class CommentContainer extends Component {
	static propTypes = {
		commentObj: PropTypes.object,
		fetchComments: PropTypes.func,
		postId: PropTypes.string,
	};

	componentDidMount() {
		if (!(this.props.commentObj.comments && this.props.commentObj.comments.length > 0)) {
			this.props.fetchComments(this.props.postId);
		}
	}

	renderCommentList = (comments) => {
		return comments && comments.map(comment => <CommentItem key={comment.id} comment={comment} />);
	};

	render() {
		const { commentObj } = this.props;
		if (commentObj && commentObj.isFetching) {
			return (
				<div>Loading...</div>
			);
		}

		if (!commentObj.comments || (commentObj.comments && commentObj.comments.length === 0)) {
			return (
				<div>Comments not found!</div>
			);
		}

		return (
			<div className="CommentList">
				<h5>Comments:</h5>
				{this.renderCommentList(commentObj.comments)}
			</div>
		);
	}
}

const mapActionCreators = {
	fetchComments: fetchComments,
};

const mapStateToProps = (state) => ({
	commentObj: state.comments,
});

export default connect(mapStateToProps, mapActionCreators)(CommentContainer);
