import React from 'react';

import PostContainer from '../../container/post-container';

function PostPage({ match }) {
	return (
		<div className="PageManagement">
			<h2>Post Management</h2>
			<PostContainer match={match} />
		</div>
	);
}

export default PostPage;
