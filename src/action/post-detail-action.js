import * as dataService from '../util/api';

// ACTION TYPES
export const REQUEST_POSTS_DETAIL = 'REQUEST_POSTS_DETAIL';
export const RECEIVE_POSTS_DETAIL = 'RECEIVE_POSTS_DETAIL';
export const ERROR_POSTS_DETAIL = 'ERROR_POSTS_DETAIL';

// ACTION CREATORS
export const requestPostDetail = () => ({
	type: REQUEST_POSTS_DETAIL,
});

export const receivePostDetail = (posts) => ({
	type: RECEIVE_POSTS_DETAIL,
	posts,
});

export const postDetailError = (postDetailError) => ({
	type: ERROR_POSTS_DETAIL,
	postDetailError,
});

// FETCHING
export const fetchPostDetail = postId => dispatch => {
	dispatch(requestPostDetail());
	return dataService.apiGet(`/posts/${postId}`)
		.then(response => dispatch(receivePostDetail(response)))
		.catch(err => dispatch(postDetailError(err)));
};
