import * as dataService from '../util/api';

// ACTION TYPES
export const REQUEST_POSTS = 'REQUEST_POSTS';
export const RECEIVE_POSTS = 'RECEIVE_POSTS';
export const ERROR_POSTS = 'ERROR_POSTS';

export const REQUEST_DELETE_POST = 'REQUEST_DELETE_POST';
export const RECEIVE_DELETE_POST = 'RECEIVE_DELETE_POST';
export const ERROR_DELETE_POST = 'ERROR_DELETE_POST';

// ACTION CREATORS
// get list post
export const requestPosts = () => ({
	type: REQUEST_POSTS,
});

export const receivePosts = (posts) => ({
	type: RECEIVE_POSTS,
	posts,
});

export const postError = (postError) => ({
	type: ERROR_POSTS,
	postError,
});

// delete post by id
export const requestDeletePost = () => ({
	type: REQUEST_DELETE_POST,
});

export const receiveDeletePost = (postId) => ({
	type: RECEIVE_DELETE_POST,
	postId,
});

export const deletePostError = (deletePostError) => ({
	type: ERROR_DELETE_POST,
	deletePostError,
});

// FETCHING
export const fetchPosts = userId => dispatch => {
	let urlReq = '/posts';
	// fetch post by user id
	if (userId) {
		urlReq += `?userId=${userId}`;
	}
	dispatch(requestPosts());
	return dataService.apiGet(urlReq)
		.then(response => dispatch(receivePosts(response)))
		.catch(err => dispatch(postError(err)));
};

export const deletePosts = postId => dispatch => {
	dispatch(requestDeletePost());
	return dataService.apiDelete(`/posts/${postId}`)
		.then(response => dispatch(receiveDeletePost(postId)))
		.catch(err => dispatch(deletePostError(err)));
};
