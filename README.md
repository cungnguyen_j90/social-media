# Technical Challenge
A simple React application. This application is for **technical challenge purpose** only.
[Live Demo](https://ancient-coast-83213.herokuapp.com/)

**Install**

run `npm install`

**Run application**

Local:

run `npm run dev`

Test:

run `npm run test`

Build for production:

run `npm run prod`

**Dependencies**

Please see `package.json` for all dependencies

**Node and npm version**

During implement this application. My version of: 

- window: 10
- node: v7.5.0
- npm: v4.1.2

# TODO
- Minifi CSS in production
- Handle loading, error state (when call API)

#Author
- Name: Cung Nguyen
- Email: hoangcung.k52@gmail.com

