import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import * as routingConst from '../constant/route-constant';
import { fetchPosts, deletePosts } from '../action/post-action';
import Table from '../component/table';

/**
 * TODO Paging
 */
class PostContainer extends Component {
	static propTypes = {
		postObj: PropTypes.object,
		fetchPost: PropTypes.func,
		deletePosts: PropTypes.func,
		match: PropTypes.object,
	};

	componentDidMount() {
		this.props.fetchPost(this.props.match.params.userId);
	}

	handleDeleteClick = (id) => {
		const res = confirm('Are you sure you want to delete this post?');
		if (res) {
			this.props.deletePosts(id);
		}
	}

	renderPostList = (posts) => {
		return posts && posts.map((post) => {
			return (
				<tr key={post.id}>
					<th scope="row">{post.id}</th>
					<td>{post.userId}</td>
					<td>{post.title}</td>
					<td>
						<Link to={`${routingConst.POST_DETAIL_MANAGEMENT_STR}/${post.id}`}>
							Detail
						</Link> |
						<a href="javascript:void(0);" className="Danger" onClick={() => this.handleDeleteClick(post.id)}> Delete</a>
					</td>
				</tr>
			)
		});
	};

	render() {
		const { postObj } = this.props;
		if (postObj && postObj.isFetching) {
			return (
				<div>Loading...</div>
			);
		}

		if (!postObj.posts || (postObj.posts && postObj.posts.length === 0)) {
			return (
				<div>Post not found!</div>
			);
		}

		return (
			<div className="PostManagement">
				{/*<div className="FilterManagement">

				</div>*/}
				<Table>
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">User ID</th>
							<th scope="col">Title</th>
							<th scope="col"/>
						</tr>
					</thead>
					<tbody className="table-hover">
						{this.renderPostList(postObj.posts)}
					</tbody>
				</Table>
			</div>
		);
	}
}

const mapActionCreators = {
	fetchPost: fetchPosts,
	deletePosts: deletePosts,
};

const mapStateToProps = (state) => ({
	postObj: state.posts,
});

export default connect(mapStateToProps, mapActionCreators)(PostContainer);
