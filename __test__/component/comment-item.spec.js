import React from 'react';
import CommentItem from '../../src/component/comment-item';
import { shallow } from 'enzyme';

describe('CommentItem Shallow Render REACT COMPONENTS', () => {
	let wrapper;
	const comment = {
		postId: 1,
		id: 1,
		name: "id labore ex et quam laborum",
		email: "Eliseo@gardner.biz",
		body: "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium"
	};

	beforeEach(() => {
		wrapper = shallow(<CommentItem comment={comment}/>)
	});

	it('Must render component', () => {
		expect(wrapper.length).toEqual(1)
	});
});