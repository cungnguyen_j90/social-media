import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchPostDetail } from '../action/post-detail-action';
import Table from '../component/table';
import CommentContainer from './comment-container';

class PostDetailContainer extends Component {
	static propTypes = {
		postDetailObj: PropTypes.object,
		fetchPostDetail: PropTypes.func,
		match: PropTypes.object,
	};

	componentDidMount() {
		if (this.props.match.params.postId) {
			this.props.fetchPostDetail(this.props.match.params.postId);
		}
	}

	renderPostDetail = (postDetail) => {
		return postDetail && Object.keys(postDetail).map((key) => {
			return (
				<tr key={key}>
					<th scope="row">{key}</th>
					<td>{postDetail[key]}</td>
				</tr>
			)
		});
	}

	render() {
		const { postDetailObj } = this.props;
		if (postDetailObj && postDetailObj.isFetching) {
			return (
				<div>Loading...</div>
			);
		}

		if (!postDetailObj.postDetail || (postDetailObj.postDetail && Object.keys(postDetailObj.postDetail).length === 0)) {
			return (
				<div>Post with ID#{this.props.match.params.postId} not found!</div>
			);
		}

		return (
			<div className="PostManagement">
				<Table>
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Information</th>
						</tr>
					</thead>
					<tbody className="table-hover">
						{this.renderPostDetail(postDetailObj.postDetail)}
					</tbody>
				</Table>
				<div className="CommentWrapper">
					<div className="CommentBox">

					</div>
					<CommentContainer postId={this.props.match.params.postId} />
				</div>
			</div>
		);
	}
}

const mapActionCreators = {
	fetchPostDetail,
};

const mapStateToProps = (state) => ({
	postDetailObj: state.postDetail,
});

export default connect(mapStateToProps, mapActionCreators)(PostDetailContainer);
