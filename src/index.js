import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './routes';

import './style/index.scss';

ReactDOM.render(
	<Routes />,
	window.document.getElementById('root'),
);
