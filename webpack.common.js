const webpack = require('webpack');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './src/index.html',
    filename: 'index.html',
    inject: 'body'
});

const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const ExtractTextWebpackPluginConfig = new ExtractTextWebpackPlugin({
    filename: '[name].[chunkhash].css'
});

const plugins = [
    HtmlWebpackPluginConfig,
    ExtractTextWebpackPluginConfig
];

const rules = [
    {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
            loader: "babel-loader"
        }
    },
    {
        test: /\.(s*)css$/,
        use: ExtractTextWebpackPlugin.extract({
            fallback: 'style-loader',
            use: [
                'css-loader',
                'sass-loader'
            ]
        })
    },
    {
        test: /\.(bmp|gif|png|jp(e*)g|svg)$/,
        loader: 'url-loader',
        options: {
            limit: 10000,
            name: 'static/media/[name].[hash:8].[ext]'
        }
    },
    {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
            'file-loader'
        ]
    }
];

module.exports = {
    entry: path.join(__dirname, './src/index.js'),
    output: {
        path: path.join(__dirname, './dist'),
        filename: '[name].[chunkhash].js',
	    publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: rules,
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "vendor",
                    chunks: "initial",
                },
            },
        },
    },
    plugins: plugins
};
