import React from 'react';

import PostDetailContainer from '../../container/post-detail-container';

function PostDetailPage({ match }) {
	return (
		<div className="PageManagement">
			<h2>Post Detail Management</h2>
			<PostDetailContainer match={match} />
		</div>
	);
}

export default PostDetailPage;
