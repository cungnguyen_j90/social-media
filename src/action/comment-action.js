import * as dataService from '../util/api';

// ACTION TYPES
export const REQUEST_COMMENTS = 'REQUEST_COMMENTS';
export const RECEIVE_COMMENTS = 'RECEIVE_COMMENTS';
export const ERROR_COMMENTS = 'ERROR_COMMENTS';

// ACTION CREATORS
export const requestComments = () => ({
	type: REQUEST_COMMENTS,
});

export const receiveComments = (comments) => ({
	type: RECEIVE_COMMENTS,
	comments,
});

export const commentError = (commentError) => ({
	type: ERROR_COMMENTS,
	commentError,
});

// FETCHING
export const fetchComments = postId => dispatch => {
	dispatch(requestComments());
	return dataService.apiGet(`/comments?postId=${postId}`)
		.then(response => dispatch(receiveComments(response)))
		.catch(err => dispatch(commentError(err)));
};
