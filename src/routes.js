import React from 'react';
import { Provider } from 'react-redux';
import { Switch, Route } from 'react-router-dom'
import { ConnectedRouter } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import * as routingConst from './constant/route-constant';
import configureStore from './store/configuration-store';
import Master from './layout/master';
import IndexPage from './page/index/index.js';
import PageNotFound from './page/404';
import UserPage from './page/user';
import PostPage from './page/post';
import PostDetailPage from './page/post-detail';

export default function Routes() {
	const history = createHistory();
	const store = configureStore(history);

	return (
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<Master>
					<Switch>
						<Route exact path={routingConst.INDEX} component={IndexPage} />
						<Route path={routingConst.USER_MANAGEMENT} component={UserPage} />
						<Route exact path={routingConst.POST_MANAGEMENT} component={PostPage} />
						<Route path={routingConst.POST_BY_USER_MANAGEMENT} component={PostPage} />
						<Route path={routingConst.POST_DETAIL_MANAGEMENT} component={PostDetailPage} />
						<Route component={PageNotFound} />
					</Switch>
				</Master>
			</ConnectedRouter>
		</Provider>
	);
}
