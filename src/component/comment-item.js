import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function CommentItem({ comment, className }) {
	const cls = classNames('media', {
		'CommentItem': true,
		[className]: !!className,
	});

	return (
		<div className={cls}>
			<img className="mr-3" src="http://via.placeholder.com/64x64" alt={comment.name}/>
			<div className="media-body">
				<h5 className="mt-0">{comment.name}</h5>
				<strong className="CommentEmail">Email: {comment.email}</strong>
				<div className="CommentBody">{comment.body}</div>
			</div>
		</div>
	)
}

CommentItem.propTypes = {
	className: PropTypes.string,
	comment: PropTypes.object,
};
