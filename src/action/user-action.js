import * as dataService from '../util/api';

// ACTION TYPES
export const REQUEST_USERS = 'REQUEST_USERS';
export const RECEIVE_USERS = 'RECEIVE_USERS';
export const ERROR_USERS = 'ERROR_USERS';

// ACTION CREATORS
export const requestUsers = users => ({
	type: REQUEST_USERS,
	users,
});

export const receiveUsers = (users) => ({
	type: RECEIVE_USERS,
	users,
});

export const userError = (userError) => ({
	type: ERROR_USERS,
	userError,
});

// FETCHING
export const fetchUsers = users => dispatch => {
	dispatch(requestUsers(users));
	// TODO Should handle error display when can not get user list
	return dataService.apiGet('/users')
		.then(response => dispatch(receiveUsers(response)))
		.catch(err => dispatch(userError(err)));
};
