import {
	REQUEST_COMMENTS,
	RECEIVE_COMMENTS,
	ERROR_COMMENTS,
} from '../action/comment-action';

const initialState = {
	isFetching: false,
	comments: [],
};

export default function commentsReducer(state = initialState, action) {
	switch (action.type) {
		case REQUEST_COMMENTS:
			return {
				...state,
				isFetching: true,
			};
		case RECEIVE_COMMENTS:
			return {
				...state,
				isFetching: false,
				comments: action.comments,
			};
		case ERROR_COMMENTS:
			return {
				...state,
				isFetching: false,
				commentError: action.commentError,
			};
		default:
			return state;
	}
}
