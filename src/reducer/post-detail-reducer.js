import {
	REQUEST_POSTS_DETAIL,
	RECEIVE_POSTS_DETAIL,
	ERROR_POSTS_DETAIL,
} from '../action/post-detail-action';

const initialState = {
	isFetching: false,
	postDetail: [],
};

export default function postDetailReducer(state = initialState, action) {
	switch (action.type) {
		case REQUEST_POSTS_DETAIL:
			return {
				...state,
				isFetching: true,
			};
		case RECEIVE_POSTS_DETAIL:
			return {
				...state,
				isFetching: false,
				postDetail: action.posts,
			};
		case ERROR_POSTS_DETAIL:
			return {
				...state,
				isFetching: false,
				postDetailError: action.postDetailError,
			};
		default:
			return state;
	}
}
