import axios from 'axios';

axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com/';

function sendRequestToUrl(api, method, data, headers) {
	return new Promise((resolve, reject) => {
		axios({
			method: method,
			headers: headers || {},
			url: api,
			data: data || {}
		}).then((response) => {
			resolve(response.data, response.status);
		}).catch((error) => {
			let e = { ...error };
			reject(e.response && e.response.data);
		});
	});
}

function apiPost(api, data, headers) {
	return sendRequestToUrl(api, 'post', data, headers);
}

function apiGet(api) {
	return sendRequestToUrl(api, 'get', {}, {
		'Content-Type': 'application/json'
	});
}

function apiDelete(api, data, headers) {
	return sendRequestToUrl(api, 'delete', data, headers);
}

export {
	apiPost,
	apiGet,
	apiDelete,
}
