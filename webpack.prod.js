const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const UglifyJSPluginConfig = new UglifyJSPlugin({
    sourceMap: true
});

const DefinePluginConfig = new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('production')
});

const HashedModuleIdsPluginConfig = new webpack.HashedModuleIdsPlugin();

const CleanWebpackPlugin = require('clean-webpack-plugin');
const CleanWebpackPluginConfig = new CleanWebpackPlugin(['dist']);

const plugins = [
    UglifyJSPluginConfig,
    DefinePluginConfig,
    DefinePluginConfig,
    HashedModuleIdsPluginConfig,
    CleanWebpackPluginConfig,
];

module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    plugins: plugins
});
