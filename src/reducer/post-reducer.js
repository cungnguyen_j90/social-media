import {
	REQUEST_POSTS,
	RECEIVE_POSTS,
	ERROR_POSTS,
	REQUEST_DELETE_POST,
	RECEIVE_DELETE_POST,
	ERROR_DELETE_POST,
} from '../action/post-action';

const initialState = {
	isFetching: false,
	posts: [],
};

export default function postsReducer(state = initialState, action) {
	switch (action.type) {
		case REQUEST_POSTS:
			return {
				...state,
				isFetching: true,
			};
		case RECEIVE_POSTS:
			return {
				...state,
				isFetching: false,
				posts: action.posts,
			};
		case ERROR_POSTS:
			return {
				...state,
				isFetching: false,
				postError: action.postError,
			};
		case REQUEST_DELETE_POST:
			return {
				...state,
				isFetching: true,
			};
		case RECEIVE_DELETE_POST:
			return {
				...state,
				isFetching: false,
				posts: state.posts && state.posts.filter(p => p.id !== action.postId),
			};
		case ERROR_DELETE_POST:
			return {
				...state,
				isFetching: false,
				deletePostError: action.deletePostError,
			};
		default:
			return state;
	}
}
