import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import * as routingConst from '../constant/route-constant';
import { fetchUsers } from '../action/user-action';
import Table from '../component/table';

class UserContainer extends Component {
	static propTypes = {
		userObj: PropTypes.object,
		fetchUsers: PropTypes.func,
	};

	componentDidMount() {
		this.props.fetchUsers();
	}

	renderUserList = () => {
		return this.props.userObj.users && this.props.userObj.users.map((user) => {
			return (
				<tr key={user.id}>
					<th scope="row">{user.id}</th>
					<td>{user.name}</td>
					<td>{user.email}</td>
					<td>
						<Link to={`${routingConst.POST_BY_USER_MANAGEMENT_STR}/${user.id}`}>
							View Posts
						</Link>
					</td>
				</tr>
			)
		});
	};

	render() {
		const { userObj } = this.props;
		if (userObj && userObj.isFetching) {
			return (
				<div>Loading...</div>
			);
		}

		if (!userObj.users || (userObj.users && userObj.users.length === 0)) {
			return (
				<div>User not found!</div>
			);
		}

		return (
			<Table>
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Name</th>
						<th scope="col">Email</th>
						<th scope="col"/>
					</tr>
				</thead>
				<tbody className="table-hover">
					{this.renderUserList()}
				</tbody>
			</Table>
		);
	}
}

const mapActionCreators = {
	fetchUsers,
};

const mapStateToProps = (state) => ({
	userObj: state.users,
});

export default connect(mapStateToProps, mapActionCreators)(UserContainer);