import React from 'react';

function IndexPage() {
	return (
		<div className="alert alert-primary" role="alert">
			Social Media Dashboard
		</div>
	);
}

export default IndexPage;
