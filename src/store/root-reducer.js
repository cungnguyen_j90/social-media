import { combineReducers } from "redux";
import { routerReducer as router } from 'react-router-redux';

import usersReducer from '../reducer/user-reducer';
import postsReducer from '../reducer/post-reducer';
import postDetailReducer from '../reducer/post-detail-reducer';
import commentsReducer from '../reducer/comment-reducer';

// TODO find the way to automatic add the reducers
export const rootReducer = () => {
	return combineReducers({
		router,
		users: usersReducer,
		posts: postsReducer,
		postDetail: postDetailReducer,
		comments: commentsReducer,
	});
};

export default rootReducer;
