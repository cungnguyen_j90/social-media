import React from 'react';
import { NavLink } from 'react-router-dom';

import * as routingConst from '../constant/route-constant';

export default function Navigator(props) {
	return (
		<nav className="PageNavigator">
			<ul className="PageNavigatorList">
				<li className="PageNavigatorItem">
					<NavLink to={routingConst.INDEX} exact className="PageNavigatorLink">
						<i className="fas fa-home PageNavigatorItemIcon" />
						<span className="PageNavigatorItemText">Dashboard</span>
					</NavLink>
				</li>
				<li className="PageNavigatorItem">
					<NavLink to={routingConst.USER_MANAGEMENT} exact className="PageNavigatorLink">
						<i className="fas fa-users PageNavigatorItemIcon" />
						<span className="PageNavigatorItemText">User Management</span>
					</NavLink>
				</li>
				<li className="PageNavigatorItem">
					<NavLink to={routingConst.POST_MANAGEMENT} className="PageNavigatorLink">
						<i className="fas fa-newspaper PageNavigatorItemIcon" />
						<span className="PageNavigatorItemText">Post Management</span>
					</NavLink>
				</li>
			</ul>
		</nav>
	);
}