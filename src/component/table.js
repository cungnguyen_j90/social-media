import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

/**
 * This is bootstrap table
 * TODO should handle the way to append header, row (can based on array)
 * @param props
 * @returns {*}
 * @constructor
 */
export default function Table(props) {
	const { className, ...restProps } = props;
	const cls = classNames('table', {
		[className]: !!className,
	});

	return (
		<table className={cls} {...restProps}>
			{props.children}
		</table>
	)
}

Table.propTypes = {
	className: PropTypes.string,
	children: PropTypes.node,
}
