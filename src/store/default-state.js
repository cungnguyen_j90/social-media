export default {
	users: {
		isFetching: false,
	},
	posts: {
		isFetching: false,
	},
	postDetail: {
		isFetching: false,
	},
	comments: {
		isFetching: false,
	},
};
