import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';

import rootReducer from './root-reducer';
import defaultState from './default-state';

export default function configureStore(history) {
	const middleware = [thunk, routerMiddleware(history)];

	const reduxDEC = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
	const composeEnhancers = (process.env.NODE_ENV !== 'production' && reduxDEC) ? reduxDEC({}) : compose;
	return createStore(
		rootReducer(),
		defaultState,
		composeEnhancers(
			applyMiddleware(...middleware),
		)
	)
};
