import React from 'react';

export default function Header(props) {
	return (
		<header className="PageHeader container-fluid">
			<div className="row PageHeaderTopBar">
				<div className="col-2 PageHeaderTopBarBrand">
					<span>Your Logo</span>
				</div>
				<div className="col-10 PageHeaderTopBarList">
					Lorem
				</div>
			</div>
		</header>
	);
}